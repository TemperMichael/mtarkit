# MTARKit

## Information

When using Augmented Reality features do not forget to set a description for the `Privacy - Camera Usage Description` in the Info.plist file. 

If you want to use Systems of the package, like the LookAtSystem, you have to register them first. For example in the main app file:

```swift
@main
struct MTExamplesApp: App {

    init() {
        LookAtSystem.registerSystem()
    }

    // ...
}
```
