//
//  HasAudio.swift
//  
//
//  Created by Michael Temper on 23.05.23.
//

import RealityKit

public protocol HasAudio where Self: Entity {}

extension HasAudio {
        
    public var audioComponent: AudioComponent {
        get { components[AudioComponent.self] ?? AudioComponent() }
        set { components[AudioComponent.self] = newValue }
    }

    public func addAudio(_ audioFile: ARAudioFileResource?, name: String) {
        guard let audioFile = audioFile else { return }
        audioComponent.audioResources[name] = audioFile
    }

    public func playAudio(name: String?, gain: Double = .zero) {
        guard let name = name else {
            print("Given audio name is nil")
            return
        }

        guard let audio = audioComponent.audioResources[name] else {
            print("Audio was not found: \(name)")
            return
        }

        guard (audio.kind == .music && audioComponent.isMusicEnabled) || (audio.kind == .sound && audioComponent.isSoundEnabled) else {
            return
        }

        let audioController = prepareAudio(audio.resource)
        audioController.gain = audioComponent.audioGainPercentage?(gain) ?? gain
        audioComponent.playbackController[name] = audioController
        audioController.play()
    }

    public func pauseAudio(name: String?) {
        guard let name = name else {
            print("Given audio name is nil")
            return
        }

        audioComponent.playbackController[name]?.pause()
    }

    public func continueAudio(name: String?, gain: Double = .zero) {
        guard let name = name else {
            print("Given audio name is nil")
            return
        }
        
        guard let audio = audioComponent.audioResources[name] else {
            print("Audio was not found: \(name)")
            return
        }

        guard (audio.kind == .music && audioComponent.isMusicEnabled) || (audio.kind == .sound && audioComponent.isSoundEnabled) else {
            return
        }

        if let playbackController = audioComponent.playbackController[name] {
            playbackController.play()
        } else {
            playAudio(name: name, gain: gain)
        }
    }
    
    public func increaseAudio(by percentage: Double, duration: Double) {
        audioComponent.audioGainPercentage = { gain in
            gain * (1 - percentage)
        }
        audioComponent.playbackController.values.forEach {
            guard $0.gain != .zero else { return }
            let finalVolume = $0.gain * (1 - percentage)
            $0.fade(to: finalVolume, duration: duration)
        }
    }
    
    public func decreaseAudio(by percentage: Double, duration: Double) {
        audioComponent.audioGainPercentage = { gain in
            gain == .zero ? -40 * percentage : gain * (1 / (1 - percentage))
        }
        
        audioComponent.playbackController.values.forEach {
            let finalVolume = $0.gain == .zero ? -40 * percentage : $0.gain * (1 / (1 - percentage))
            $0.fade(to: finalVolume, duration: duration)
        }
    }
}
