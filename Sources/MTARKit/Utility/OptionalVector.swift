//
//  OptionalVector.swift
//  PloppyMatchingPairs
//
//  Created by Michael Temper on 29.08.23.
//

import Foundation

public struct OptionalVector: Codable {
    let x: Float?
    let y: Float?
    let z: Float?

    public init(x: Float? = nil, y: Float? = nil, z: Float? = nil) {
        self.x = x
        self.y = y
        self.z = z
    }
}
