//
//  HasLookAt.swift
//  
//
//  Created by Michael Temper on 24.04.23.
//

import RealityKit

public protocol HasLookAt {}

extension HasLookAt where Self: Entity {

    public var lookAt: LookAtComponent {
        get {
            components[LookAtComponent.self] ?? LookAtComponent(isActive: true,
                                                                lookDestination: .zero,
                                                                deferredRotationAngle: 0,
                                                                deferredRotationAxis: [0, 1, 0])
        }

        set { components[LookAtComponent.self] = newValue }
    }

    public var isLookAtComponentActive: Bool {
        get { lookAt.isActive }
        set { lookAt.isActive = newValue }
    }

    public var lookDestination: SIMD3<Float> {
        get { lookAt.lookDestination }
        set { lookAt.lookDestination = newValue }
    }

    public var deferredRotationAngle: Float {
        get { lookAt.deferredRotationAngle }
        set { lookAt.deferredRotationAngle = newValue }
    }

    public var deferredRotationAxis: SIMD3<Float> {
        get { lookAt.deferredRotationAxis }
        set { lookAt.deferredRotationAxis = newValue }
    }
}
