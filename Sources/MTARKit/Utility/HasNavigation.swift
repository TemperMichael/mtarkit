//
//  File.swift
//  
//
//  Created by Michael Temper on 10.06.23.
//

import RealityKit

public protocol HasNavigation {}

extension HasNavigation where Self: Entity {

    public var navigationSystem: NavigationComponent {
        get { components[NavigationComponent.self] ?? NavigationComponent(isActive: true) }
        set { components[NavigationComponent.self] = newValue }
    }

    public var isNavigationComponentActive: Bool {
        get { navigationSystem.isActive }
        set { navigationSystem.isActive = newValue }
    }

    public var currentCameraTransform: simd_float4x4 {
        get { navigationSystem.currentCameraTransform }
        set { navigationSystem.currentCameraTransform = newValue }
    }
}
