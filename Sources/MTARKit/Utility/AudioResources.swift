//
//  AudioResources+Extensions.swift
//  
//
//  Created by Michael Temper on 22.05.23.
//

import AVFoundation
import Combine
import RealityKit

public class AudioResources {

#if os(visionOS)
    public static func loadAudio(_ file: ARAudioFile) -> ARAudioFileResource {
        do {
            let resource = try AudioFileResource.load(named: file.name, configuration: AudioFileResource.Configuration(shouldLoop: file.loop))
            return ARAudioFileResource(resource: resource, kind: file.kind)
        } catch {
            fatalError("Failed to load audio: \(file.name)")
        }
    }
#else
    public static func loadAudioAsync(_ file: ARAudioFile) -> AnyPublisher<ARAudioFileResource, Error> {
        guard let url = Bundle.main.url(forResource: file.name, withExtension: file.type.rawValue) else {
            fatalError("Audio file not found in bundle: \(file.name)")
        }

        return AudioFileResource.loadAsync(contentsOf: url,
                                           withName: file.fullName,
                                           inputMode: file.mode,
                                           loadingStrategy: .preload,
                                           shouldLoop: file.loop)
        .tryMap { resource in
            return ARAudioFileResource(resource: resource, kind: file.kind)
        }
        .eraseToAnyPublisher()
    }
#endif
}
