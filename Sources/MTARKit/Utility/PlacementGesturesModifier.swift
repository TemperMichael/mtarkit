//
//  PlacementGesturesModifier.swift
//  
//
//  Created by Michael Temper on 27.08.23.
//

import SwiftUI
import RealityKit
import ARKit

#if os(iOS)
public struct PlacementContainer: Codable, Equatable {
    public var scale: Double
    public var startScale: Double?
    public var currentRotation: SIMD3<Float>
    public var finalRotation: SIMD3<Float>
    public var position: SIMD3<Float>
    public var startPosition: SIMD3<Float>?
    public var initialPosition: SIMD3<Float>?
    
    public init(scale: Double = 1,
                startScale: Double? = nil,
                currentRotation: SIMD3<Float> = .zero,
                finalRotation: SIMD3<Float> = .zero,
                position: SIMD3<Float> = .zero,
                startPosition: SIMD3<Float>? = nil,
                initialPosition: SIMD3<Float>? = nil) {
        self.scale = scale
        self.startScale = startScale
        self.currentRotation = currentRotation
        self.finalRotation = finalRotation
        self.position = position
        self.startPosition = startPosition
        self.initialPosition = initialPosition
    }
}

#elseif os(visionOS)
public struct PlacementContainer: Codable, Equatable {
    public var scale: Double
    public var startScale: Double?
    public var currentRotation: Rotation3D
    public var finalRotation: Rotation3D
    public var position: SIMD3<Float>
    public var startPosition: SIMD3<Float>?
    public var initialPosition: SIMD3<Float>?
    
    public init(scale: Double = 1,
                startScale: Double? = nil,
                currentRotation: Rotation3D = .identity,
                finalRotation: Rotation3D = .identity,
                position: SIMD3<Float> = .zero,
                startPosition: SIMD3<Float>? = nil,
                initialPosition: SIMD3<Float>? = nil) {
        self.scale = scale
        self.startScale = startScale
        self.currentRotation = currentRotation
        self.finalRotation = finalRotation
        self.position = position
        self.startPosition = startPosition
        self.initialPosition = initialPosition
    }
}

public extension View {
    /// Listens for gestures and places an item based on those inputs.
    func placementGestures(
        placementContainer: Binding<PlacementContainer>,
        initialPosition: SIMD3<Double> = .zero,
        isActive: Bool = false
    ) -> some View {
        self.modifier(
            PlacementGesturesModifier(
                placementContainer: placementContainer, isActive: isActive
            )
        )
    }
}

/// A modifier that adds gestures and positioning to a view.
/// Attention: Use this modifier only in Windows and Volumes - in Immersive Spaces this can lead to wrong behaviours such as wrong transform in SharePlay.
private struct PlacementGesturesModifier: ViewModifier {
    @Binding var placementContainer: PlacementContainer
    
    var isActive: Bool

    func body(content: Content) -> some View {
        if isActive {
            content
                .onAppear {
                    if let initialPosition = placementContainer.initialPosition {
                        placementContainer.position = initialPosition
                    }
                }
                .rotation3DEffect(placementContainer.currentRotation * placementContainer.finalRotation)
                .scaleEffect(placementContainer.scale)
                .position(x: CGFloat(placementContainer.position.x), y: CGFloat(placementContainer.position.y))
                .offset(z: CGFloat(placementContainer.position.z))
                .simultaneousGesture(DragGesture().targetedToAnyEntity().handActivationBehavior(.pinch)
                    .onChanged { value in
                        if let startPosition = placementContainer.startPosition {
                            let delta = value.convert(value.location3D, from: .local, to: .scene) - value.convert(value.startLocation3D, from: .local, to: .scene)
                            placementContainer.position = startPosition + delta
                        } else {
                            placementContainer.startPosition = placementContainer.position
                        }
                        
                    }
                    .onEnded { _ in
                        placementContainer.startPosition = nil
                    }
                )
                .simultaneousGesture(RotateGesture3D(constrainedToAxis: .y).handActivationBehavior(.pinch)
                    .onChanged { value in
                        placementContainer.currentRotation = value.rotation
                    }
                    .onEnded { value in
                        placementContainer.finalRotation *= placementContainer.currentRotation
                        
                        placementContainer.currentRotation = .identity
                    }
                )
                .simultaneousGesture(MagnifyGesture().handActivationBehavior(.pinch)
                    .onChanged { value in
                        if let startScale = placementContainer.startScale {
                            placementContainer.scale = max(0.3, min(3, value.magnification * startScale))
                        } else {
                            placementContainer.startScale = placementContainer.scale
                        }
                    }
                    .onEnded { value in
                        placementContainer.startScale = placementContainer.scale
                    }
                )
        } else {
            content
        }
    }
}
#endif
