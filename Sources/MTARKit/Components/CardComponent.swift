//
//  CardComponent.swift
//
//
//  Created by Michael Temper on 05.04.23.
//

import RealityKit
import UIKit

public struct CardComponent: Component {
    public var id: Int
    public var isFaceUp: Bool
    public var isMatched: Bool
    public var isBottomImageVisible: Bool

    public var topImageName: String
    public var bottomImageName: String
    public var floatingImageName: String?

    public var width: Float
    public var height: Float
    public var depth: Float
    public var cornerRadius: Float

    public var position: SIMD3<Float>
    public var orientation: Orientation
    public var scale: SIMD3<Float>

    @CodableColor public var borderColor: UIColor
    @CodableColor public var topImageColor: UIColor
    @CodableColor public var floatingImageColor: UIColor

    public var selectionSound: ARAudioFile?
    public var matchingSound: ARAudioFile?

    public var accessibilityLabel: LocalizedStringResource?
    public var imageAccessibilityLabel: LocalizedStringResource?

#if os(visionOS)
    public var celebrationParticlesComponent: ParticleEmitterComponent?
    public var hideParticlesComponent: ParticleEmitterComponent?

    public init(id: Int = 0,
                isFaceUp: Bool = false,
                isMatched: Bool = false,
                isBottomImageVisible: Bool = true,
                topImageName: String = "",
                bottomImageName: String = "",
                floatingImageName: String? = nil,
                width: Float = 0.05,
                height: Float = 0.005,
                depth: Float = 0.05,
                cornerRadius: Float = 0,
                position: SIMD3<Float> = .zero,
                orientation: Orientation = .horizontal,
                scale: SIMD3<Float> = [1, 1, 1],
                borderColor: UIColor = .white,
                topImageColor: UIColor = .white,
                floatingImageColor: UIColor = .white,
                selectionSound: ARAudioFile? = nil,
                matchingSound: ARAudioFile? = nil,
                accessibilityLabel: LocalizedStringResource? = nil,
                imageAccessibilityLabel: LocalizedStringResource? = nil,
                celebrationParticlesComponent: ParticleEmitterComponent? = nil,
                hideParticlesComponent: ParticleEmitterComponent? = nil
    ) {
        self.init(
            id: id,
            isFaceUp: isFaceUp,
            isMatched: isMatched,
            isBottomImageVisible: isBottomImageVisible,
            topImageName: topImageName,
            bottomImageName: bottomImageName,
            floatingImageName: floatingImageName,
            width: width,
            height: height,
            depth: depth,
            cornerRadius: cornerRadius,
            position: position,
            orientation: orientation,
            scale: scale,
            borderColor: borderColor,
            topImageColor: topImageColor,
            floatingImageColor: floatingImageColor,
            selectionSound: selectionSound,
            matchingSound: matchingSound,
            accessibilityLabel: accessibilityLabel,
            imageAccessibilityLabel: imageAccessibilityLabel
        )

        self.celebrationParticlesComponent = celebrationParticlesComponent
        self.hideParticlesComponent = hideParticlesComponent
    }
#endif

    public init(id: Int = 0,
                isFaceUp: Bool = false,
                isMatched: Bool = false,
                isBottomImageVisible: Bool = true,
                topImageName: String = "",
                bottomImageName: String = "",
                floatingImageName: String? = nil,
                width: Float = 0.05,
                height: Float = 0.005,
                depth: Float = 0.05,
                cornerRadius: Float = 0,
                position: SIMD3<Float> = .zero,
                orientation: Orientation = .horizontal,
                scale: SIMD3<Float> = [1, 1, 1],
                borderColor: UIColor = .white,
                topImageColor: UIColor = .white,
                floatingImageColor: UIColor = .white,
                selectionSound: ARAudioFile? = nil,
                matchingSound: ARAudioFile? = nil,
                accessibilityLabel: LocalizedStringResource? = nil,
                imageAccessibilityLabel: LocalizedStringResource? = nil) {
        self.id = id
        self.isFaceUp = isFaceUp
        self.isMatched = isMatched
        self.isBottomImageVisible = isBottomImageVisible
        self.topImageName = topImageName
        self.bottomImageName = bottomImageName
        self.floatingImageName = floatingImageName
        self.width = width
        self.height = height
        self.depth = depth
        self.cornerRadius = cornerRadius
        self.position = position
        self.orientation = orientation
        self.scale = scale
        self.borderColor = borderColor
        self.topImageColor = topImageColor
        self.floatingImageColor = floatingImageColor
        self.selectionSound = selectionSound
        self.matchingSound = matchingSound
        self.accessibilityLabel = accessibilityLabel
        self.imageAccessibilityLabel = imageAccessibilityLabel
    }
}

extension CardComponent: Equatable {

    public static func == (lhs: CardComponent, rhs: CardComponent) -> Bool {
        lhs.id == rhs.id && lhs.bottomImageName == rhs.bottomImageName
    }

    public func hasSameMesh(as otherCard: CardComponent) -> Bool {
        return width == otherCard.width && height == otherCard.height && depth == otherCard.depth && cornerRadius == otherCard.cornerRadius
    }
}
