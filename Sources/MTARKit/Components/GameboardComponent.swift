//
//  GameboardComponent.swift
//
//
//  Created by Michael Temper on 05.04.23.
//

import RealityKit

public struct GameboardComponent: Component, Codable {
    public var rows: Int
    public var columns: Int
    public var orientation: Orientation
    public var offset: OrientationOffset?

    public var gameFinishedSound: ARAudioFile?
    public var backgroundMusic: ARAudioFile?

    public init(rows: Int = 0, columns: Int = 0, orientation: Orientation = .vertical, offset: OrientationOffset? = nil, gameFinishedSound: ARAudioFile? = nil, backgroundMusic: ARAudioFile? = nil) {
        self.rows = rows
        self.columns = columns
        self.orientation = orientation
        self.offset = offset
        self.gameFinishedSound = gameFinishedSound
        self.backgroundMusic = backgroundMusic
    }
}
