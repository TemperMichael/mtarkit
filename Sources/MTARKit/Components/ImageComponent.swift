//
//  ImageComponent.swift
//
//
//  Created by Michael Temper on 07.04.23.
//

import RealityKit
import UIKit

public struct ImageComponent: Component, Identifiable, Equatable, Codable {
    public var id: String
    public var imageName: String
    public var width: Float
    public var height: Float
    public var padding: CGFloat

    @CodableColor public var color: UIColor

    public init(id: String = UUID().uuidString,
                imageName: String,
                width: Float = 0.05,
                height: Float = 0.05,
                padding: CGFloat = 20,
                color: UIColor = .white) {
        self.id = id
        self.imageName = imageName
        self.width = width
        self.height = height
        self.padding = padding
        self.color = color
    }

    public static func == (lhs: ImageComponent, rhs: ImageComponent) -> Bool {
        lhs.id == rhs.id
    }
}
