//
//  PivotComponent.swift
//
//
//  Created by Michael Temper on 07.04.23.
//

import RealityKit

public struct PivotComponent: Component, Codable {
    public var point: PivotPoint

    public init(point: PivotPoint = .centerCenterCenter) {
        self.point = point
    }
}
