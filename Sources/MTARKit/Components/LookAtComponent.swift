//
//  LookAtComponent.swift
//  
//
//  Created by Michael Temper on 24.04.23.
//

import RealityKit

public struct LookAtComponent: Component {
    public var isActive: Bool
    public var lookDestination: SIMD3<Float>
    public var deferredRotationAngle: Float
    public var deferredRotationAxis: SIMD3<Float>

    public init(isActive: Bool, lookDestination: SIMD3<Float>, deferredRotationAngle: Float, deferredRotationAxis: SIMD3<Float>) {
        self.isActive = isActive
        self.lookDestination = lookDestination
        self.deferredRotationAngle = deferredRotationAngle
        self.deferredRotationAxis = deferredRotationAxis
    }
}
