//
//  File.swift
//  
//
//  Created by Michael Temper on 22.05.23.
//

import RealityKit

public struct AudioComponent: Component {
    public var isMusicEnabled: Bool
    public var isSoundEnabled: Bool
    public var audioResources: [String: ARAudioFileResource]
    public var audioGainPercentage: ((Double) -> Double)?

    var playbackController: [String: AudioPlaybackController] = [:]

    public init(isMusicEnabled: Bool = true, isSoundEnabled: Bool = true, audioResources: [String: ARAudioFileResource] = [:], audioGainPercentage: ((Double) -> Double)? = nil) {
        self.isMusicEnabled = isMusicEnabled
        self.isSoundEnabled = isSoundEnabled
        self.audioResources = audioResources
        self.audioGainPercentage = audioGainPercentage
    }
}
