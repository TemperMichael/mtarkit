//
//  NavigationComponent.swift
//  
//
//  Created by Michael Temper on 10.06.23.
//

import RealityKit

public struct NavigationComponent: Component {
    public var isActive: Bool
    public var currentCameraTransform: simd_float4x4
    public var visualNavigationEntity: ModelEntity?

    public init(isActive: Bool, currentCameraTransform: simd_float4x4 = matrix_float4x4(), visualNavigationElement: ModelEntity? = nil) {
        self.isActive = isActive
        self.currentCameraTransform = currentCameraTransform
        self.visualNavigationEntity = visualNavigationElement
    }
}
