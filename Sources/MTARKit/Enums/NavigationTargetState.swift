//
//  NavigationTargetState.swift
//  
//
//  Created by Michael Temper on 10.06.23.
//

import Foundation

enum NavigationTargetState {
    case visible
    case right
    case left
    case up
    case down
    case away
    case tooClose

    var accessibilityAnnouncementIdentifier: String {
        switch self {
        case .visible:
            return "accessibility.navigationSystem.gameboard.visible.announcement"
        case .right:
            return "accessibility.navigationSystem.rotate.left.announcement"
        case .left:
            return "accessibility.navigationSystem.rotate.right.announcement"
        case .up:
            return "accessibility.navigationSystem.rotate.down.announcement"
        case .down:
            return "accessibility.navigationSystem.rotate.up.announcement"
        case .away:
            return "accessibility.navigationSystem.move.closer.announcement"
        case .tooClose:
            return "accessibility.navigationSystem.move.away.announcement"
        }
    }
}
