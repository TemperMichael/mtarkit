//
//  ARAudioFileType.swift
//  
//
//  Created by Michael Temper on 23.05.23.
//

import Foundation

public enum ARAudioFileType: String, Codable {
    case wav
    case mp3
}
