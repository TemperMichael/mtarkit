//
//  ARAudioKind.swift
//  
//
//  Created by Michael Temper on 23.05.23.
//

import Foundation

public enum ARAudioKind: String, Codable {
    case sound
    case music
}
