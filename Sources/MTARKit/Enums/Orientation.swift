//
//  Orientation.swift
//  PloppyMatchingPairs
//
//  Created by Michael Temper on 27.08.23.
//

import Foundation

public enum Orientation: Codable {
    case vertical
    case horizontal
}
