//
//  PivotPoint.swift
//
//
//  Created by Michael Temper on 07.04.23.
//

import Foundation

public enum PivotPoint: Codable {
    case leadingBottomBack
    case leadingCenterBack
    case leadingTopBack

    case leadingBottomCenter
    case leadingCenterCenter
    case leadingTopCenter

    case leadingBottomFront
    case leadingCenterFront
    case leadingTopFront


    case centerBottomBack
    case centerCenterBack
    case centerTopBack

    case centerBottomCenter
    case centerCenterCenter
    case centerTopCenter

    case centerBottomFront
    case centerCenterFront
    case centerTopFront


    case trailingBottomBack
    case trailingCenterBack
    case trailingTopBack

    case trailingBottomCenter
    case trailingCenterCenter
    case trailingTopCenter

    case trailingBottomFront
    case trailingCenterFront
    case trailingTopFront

    case custom(SIMD3<Float>)
}

extension PivotPoint: RawRepresentable {

    public init?(rawValue: SIMD3<Float>) {
        switch rawValue {
        case [-1, -1, -1]:
            self = .leadingBottomBack
        case [-1, 0, -1]:
            self = .leadingCenterBack
        case [-1, 1, -1]:
            self = .leadingTopBack
        case [-1, -1, 0]:
            self = .leadingBottomCenter
        case [-1, 0, 0]:
            self = .leadingCenterCenter
        case [-1, 1, 0]:
            self = .leadingTopCenter
        case [-1, -1, 1]:
            self = .leadingBottomFront
        case [-1, 0, 1]:
            self = .leadingCenterFront
        case [-1, 1, 1]:
            self = .leadingTopFront
        case [0, -1, -1]:
            self = .centerBottomBack
        case [0, 0, -1]:
            self = .centerCenterBack
        case [0, 1, -1]:
            self = .centerTopBack
        case [0, -1, 0]:
            self = .centerBottomCenter
        case [0, 0, 0]:
            self = .centerCenterCenter
        case [0, 1, 0]:
            self = .centerTopCenter
        case [0, -1, 1]:
            self = .centerBottomFront
        case [0, 0, 1]:
            self = .centerCenterFront
        case [0, 1, 1]:
            self = .centerTopFront
        case [1, -1, -1]:
            self = .trailingBottomBack
        case [1, 0, -1]:
            self = .trailingCenterBack
        case [1, 1, -1]:
            self = .trailingTopBack
        case [1, -1, 0]:
            self = .trailingBottomCenter
        case [1, 0, 0]:
            self = .trailingCenterCenter
        case [1, 1, 0]:
            self = .trailingTopCenter
        case [1, -1, 1]:
            self = .trailingBottomFront
        case [1, 0, 1]:
            self = .trailingCenterFront
        case [1, 1, 1]:
            self = .trailingTopFront
        default:
            self = .custom(rawValue)
        }
    }

    public var rawValue: SIMD3<Float> {
        switch self {
        case .leadingBottomBack:
            return [-1, -1, -1]
        case .leadingCenterBack:
            return [-1, 0, -1]
        case .leadingTopBack:
            return [-1, 1, -1]
        case .leadingBottomCenter:
            return [-1, -1, 0]
        case .leadingCenterCenter:
            return [-1, 0, 0]
        case .leadingTopCenter:
            return [-1, 1, 0]
        case .leadingBottomFront:
            return [-1, -1, 1]
        case .leadingCenterFront:
            return [-1, 0, 1]
        case .leadingTopFront:
            return [-1, 1, 1]

        case .centerBottomBack:
            return [0, -1, -1]
        case .centerCenterBack:
            return [0, 0, -1]
        case .centerTopBack:
            return [0, 1, -1]
        case .centerBottomCenter:
            return [0, -1, 0]
        case .centerCenterCenter:
            return [0, 0, 0]
        case .centerTopCenter:
            return [0, 1, 0]
        case .centerBottomFront:
            return [0, -1, 1]
        case .centerCenterFront:
            return [0, 0, 1]
        case .centerTopFront:
            return [0, 1, 1]

        case .trailingBottomBack:
            return [1, -1, -1]
        case .trailingCenterBack:
            return [1, 0, -1]
        case .trailingTopBack:
            return [1, 1, -1]
        case .trailingBottomCenter:
            return [1, -1, 0]
        case .trailingCenterCenter:
            return [1, 0, 0]
        case .trailingTopCenter:
            return [1, 1, 0]
        case .trailingBottomFront:
            return [1, -1, 1]
        case .trailingCenterFront:
            return [1, 0, 1]
        case .trailingTopFront:
            return [1, 1, 1]
        case .custom(let point):
            return point
        }
    }

    public var x: Float { rawValue.x }
    public var y: Float { rawValue.y }
    public var z: Float { rawValue.z }
}
