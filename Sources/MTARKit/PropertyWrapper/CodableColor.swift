//
//  CodableColor.swift
//
//
//  Created by Michael Temper on 05.04.23.
//

import UIKit

@propertyWrapper
public struct CodableColor {
    public var wrappedValue: UIColor

    public init(wrappedValue: UIColor) {
        self.wrappedValue = wrappedValue
    }
}

extension CodableColor: Codable {
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let data = try container.decode(Data.self)
        guard let color = try NSKeyedUnarchiver.unarchivedObject(ofClass: UIColor.self, from: data) else {
            throw DecodingError.dataCorruptedError(
                in: container,
                debugDescription: "Invalid color"
            )
        }
        wrappedValue = color
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        let data = try NSKeyedArchiver.archivedData(withRootObject: wrappedValue, requiringSecureCoding: true)
        try container.encode(data)
    }
}
