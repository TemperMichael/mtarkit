//
//  PivotEntity.swift
//
//
//  Created by Michael Temper on 07.04.23.
//

import RealityKit

public class PivotEntity: Entity, HasModel, HasCollision {

    private var identifier = "PivotIdentifier"

    public var pivot: PivotComponent {
        get { return self.components[PivotComponent.self] ?? PivotComponent() }
        set {
            self.components[PivotComponent.self] = newValue
            setChildrenPositions()
        }
    }

    public var model: ModelComponent? {
        guard let pivotChild = children.first(where: { $0.name == identifier }) as? HasModel else {
            return components[ModelComponent.self]
        }

        return pivotChild.components[ModelComponent.self]
    }

    public init(entity: Entity, pivotPoint: PivotPoint, debug: Bool = false) {
        super.init()
        pivot.point = pivotPoint

        entity.name = identifier
        addChild(entity)

        setChildrenPositions()

        if debug {
            let debugPivotPoint = ModelEntity(mesh: .generateSphere(radius: (model?.mesh.bounds.max.max() ?? 0.1) * 0.2),
                                              materials: [UnlitMaterial(color: .blue.withAlphaComponent(0.5))])
            addChild(debugPivotPoint)
        }
    }

    @MainActor required init() {
        fatalError("init() has not been implemented")
    }

    private func setChildrenPositions() {
        guard let mesh = model?.mesh else { return }
        children.forEach { child in
            child.position = -SIMD3(x: mesh.width / 2 * pivot.point.x,
                                    y: mesh.height / 2 * pivot.point.y,
                                    z: mesh.depth / 2 * pivot.point.z)
        }
    }
}
