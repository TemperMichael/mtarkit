//
//  CardEntity.swift
//
//
//  Created by Michael Temper on 05.04.23.
//

import Combine
import Foundation
import RealityKit
import UIKit

public class CardEntity: Entity, HasModel, HasCollision, HasAudio {

    public var card: CardComponent {
        get { components[CardComponent.self] ?? CardComponent() }
        set { components[CardComponent.self] = newValue }
    }

    public var isMatched: Bool {
        get { card.isMatched }
        set {
            card.isMatched = newValue
            runMatchAnimation()
        }
    }

    public var animationCompletionHandler: Cancellable?
    public var imageEntity: ImageEntity?

    private var bottomImageMaterial: Material?
    private var topImageMaterial: Material?

    public var hideParticleEntity = Entity()
    public var celebrationParticleEntity = Entity()

    public var texture: MaterialParameters.Texture?

    public convenience init(card: CardComponent,
                            floatingImageComponent: ImageComponent? = nil,
                            cachedTextures: [String: MaterialParameters.Texture] = [:]) {
        let mesh = MeshResource.generateBox(width: card.width, height: card.height, depth: card.depth)
        self.init(card: card,
                  mesh: mesh,
                  floatingImageComponent: floatingImageComponent,
                  cachedTextures: cachedTextures)
    }

    public init(card: CardComponent,
                mesh: MeshResource,
                floatingImageComponent: ImageComponent? = nil,
                cachedTextures: [String: MaterialParameters.Texture?] = [:]) {
        super.init()
        
        self.card = card
        
        setAccessibilityText(card.accessibilityLabel)
        
        let materials = generateTexturedMaterials(for: card, cachedTextures: cachedTextures)
        model = ModelComponent(mesh: mesh, materials: materials)
        
        topImageMaterial = materials[1]
        bottomImageMaterial = materials[3]
        model?.materials[3] = materials[1]

        configure(
            with: floatingImageComponent,
            showImageAfterChange: card.isBottomImageVisible && card.isFaceUp && card.orientation == .horizontal
        )

#if os(visionOS)
        components.set(InputTargetComponent(allowedInputTypes: .all))
        components.set(HoverEffectComponent())
        components.set(GroundingShadowComponent(castsShadow: true))

        addChild(celebrationParticleEntity)
        addChild(hideParticleEntity)

        configureParticles(celebrationParticlesComponent: card.celebrationParticlesComponent,
                           hideParticlesComponent: card.hideParticlesComponent)
#endif
    }

    public func configure(with component: CardComponent, cachedTexture: MaterialParameters.Texture? = nil) {
        self.card = component
        
        setAccessibilityText(component.accessibilityLabel)
        
        if let cachedTexture {
            bottomImageMaterial = getMaterial(
                for: component.isBottomImageVisible ? cachedTexture : nil,
                color: component.isBottomImageVisible ? component.floatingImageColor : nil
            )
        } else if let imageName = component.floatingImageName {
            bottomImageMaterial = getMaterial(
                for: component.isBottomImageVisible ? getTexture(for: imageName) : nil,
                color: component.isBottomImageVisible ? component.floatingImageColor : nil,
                alpha: 0.9999999
            )
        }
        
        if let bottomImageMaterial, component.isFaceUp {
            model?.materials[3] = bottomImageMaterial
        }

        if let floatingImageName = component.floatingImageName {
            let floatingImageComponent = ImageComponent(imageName: floatingImageName,
                                                        width: component.width,
                                                        height: component.depth,
                                                        padding: 5,
                                                        color: component.floatingImageColor)
            
            // For now, no cachedTextures are used yet as there is some kind of scaling issue then
            configure(
                with: floatingImageComponent,
                cachedTexture: nil,
                showImageAfterChange: component.isBottomImageVisible && component.isFaceUp && component.orientation == .horizontal
            )
        }

        changeBorderColor(to: component.borderColor)
    }

    public func setAccessibilityText(_ label: LocalizedStringResource?) {
        if #available(iOS 17.0, *) {
            var accessibilityComponent = AccessibilityComponent()
            accessibilityComponent.isAccessibilityElement = true
            accessibilityComponent.traits = [.button, .playsSound]
            accessibilityComponent.label = label
#if os(visionOS)
            accessibilityComponent.systemActions = [.activate]
#endif
            components[AccessibilityComponent.self] = accessibilityComponent
        } else if let accessibilityLabel = label {
            isAccessibilityElement = true
            self.accessibilityLabel = String(localized: accessibilityLabel)
        }
    }

    public func configure(with floatingImageComponent: ImageComponent?, cachedTexture: MaterialParameters.Texture? = nil, showImageAfterChange: Bool = false) {
        if let imageComponent = floatingImageComponent {
            if let imageEntity {
                let playbackController = imageEntity.scale(to: .zero, duration: 1)
                imageEntity.animationCompletionHandler = imageEntity.animationCompletion { event in
                    if event.playbackController == playbackController {
                        imageEntity.changeImage(component: imageComponent, cachedTexture: cachedTexture)
                        imageEntity.position.y = imageComponent.height * -0.6
                        imageEntity.transform.rotation *= simd_quatf(angle: -.pi, axis: [1, 0, 0])
                        
                        if showImageAfterChange {
                            imageEntity.scale(to: .one, duration: 1)
                        }
                    }
                }
            } else {
                imageEntity?.removeFromParent()
                imageEntity = ImageEntity(imageComponent: imageComponent, cachedTexture: cachedTexture)
                imageEntity?.position.y = imageComponent.height * -0.6
                imageEntity?.transform.rotation *= simd_quatf(angle: -.pi, axis: [1, 0, 0])
                addChild(imageEntity!)
            }
        }
    }

#if os(visionOS)
    private func configureParticles(celebrationParticlesComponent: ParticleEmitterComponent? = nil,
                                    hideParticlesComponent: ParticleEmitterComponent? = nil) {
        celebrationParticleEntity.transform.translation = [0, 0.01, 0]
        celebrationParticleEntity.transform.rotation = simd_quatf(angle: .pi, axis: [1, 0, 0])
        celebrationParticleEntity.scale = [card.width * 10, card.height * 10, card.depth * 10]
        celebrationParticleEntity.particleEmitter = celebrationParticlesComponent

        hideParticleEntity.transform.translation = [0, -0.01, 0]
        hideParticleEntity.scale = [card.width * 7, card.height * 7, card.depth * 7]
        hideParticleEntity.particleEmitter = hideParticlesComponent
    }
#endif

    private func changeContentImage(with material: Material) {
        bottomImageMaterial = material
    }

    private func changeBorderColor(to color: UIColor) {
        var material = SimpleMaterial()
        material.color = .init(tint: color)
        model?.materials[0] = material
        model?.materials[2] = material
        model?.materials[4] = material
        model?.materials[5] = material
    }

    private func generateTexturedMaterials(for card: CardComponent, cachedTextures: [String: MaterialParameters.Texture?] = [:]) -> [RealityKit.Material] {
        var materials: [RealityKit.Material] = []
        for index in 1...6 {
            var imageName: String?
            var color: UIColor? = .white
            var alpha: Double = 1
            var texture: MaterialParameters.Texture? = nil

            switch index {
            case 2:
                imageName = card.topImageName
                color = card.topImageColor
                alpha = 0.9999999
            case 4:
                if card.isBottomImageVisible {
                    imageName = card.bottomImageName
                    color = card.floatingImageColor
                } else {
                    imageName = nil
                    color = nil
                }
                alpha = 0.9999999
            default:
                color = card.borderColor
            }

            if let imageName {
                let cachedImage: MaterialParameters.Texture? = cachedTextures[imageName] ?? nil
                texture = cachedImage ?? getTexture(for: imageName)

                if index == 4 {
                    self.texture = texture
                }
            }

            materials.append(getMaterial(for: texture, color: color, alpha: alpha))
        }

        return materials
    }

    private func getTexture(for imageName: String) -> MaterialParameters.Texture? {
        guard let image = UIImage(named: imageName)?.withPadding(36) ?? UIImage(systemName: imageName)?.withTintColor(.white, renderingMode: .alwaysOriginal).withPadding(10),
              let cgImage = image.cgImage else {
            print("Could not create CardEntity texture: \(imageName)")
            return nil
        }

        do {
            return MaterialParameters.Texture(try .generate(from: cgImage, options: .init(semantic: .hdrColor)))
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }

    private func getMaterial(for texture: MaterialParameters.Texture?, color: UIColor?, alpha: Double = 1) -> Material {
        var material = UnlitMaterial()
        if let color {
            material.color = .init(tint: color.withAlphaComponent(alpha),
                                   texture: texture)
        }
        material.blending = .transparent(opacity: PhysicallyBasedMaterial.Opacity.init(floatLiteral: texture == nil && color == nil ? 0 : 1))
        material.opacityThreshold = 0
        return material
    }

    // Used for cloning
    @MainActor required init() {}

    /*
     It seems like children get recursively copied but not the variables,
     therefore the child objects get set to their corresponding variable again
     */
    public override func didClone(from source: Entity) {
        imageEntity = children.first(where: { $0 is ImageEntity }) as? ImageEntity
    }
}

public extension CardEntity {

    @discardableResult
    func runMatchAnimation(duration: Float = 1, soundEnabled: Bool = true) -> AnimationPlaybackController {
        celebrationParticleEntity.isEnabled = true

        var firstTransform = transform
        firstTransform.rotation *= simd_quatf(angle: .pi, axis: [0, 1, 0])
        firstTransform.translation.y += 0.03
        firstTransform.scale = [1.5, 1.5, 1.5]

        var secondTransform = firstTransform
        secondTransform.rotation *= simd_quatf(angle: .pi, axis: [0, 1, 0])
        secondTransform.translation.y -= 0.03
        secondTransform.scale = .one

        var transforms: [Transform] = []
        // Without appending the original transformation, it somehow skips the first animation
        transforms.append(transform)
        transforms.append(firstTransform)
        transforms.append(secondTransform)

        let animationDefinition = SampledAnimation(frames: transforms,
                                                   frameInterval: duration / Float(transforms.count - 1),
                                                   bindTarget: .transform)
        let animationResource = try! AnimationResource.generate(with: animationDefinition)
        let playbackController = playAnimation(animationResource)

        if soundEnabled {
            playMatchingSound()
        }

#if os(visionOS)
        celebrationParticleEntity.particleEmitter?.isEmitting = true
        celebrationParticleEntity.particleEmitter?.restart()
#endif

        return playbackController
    }

    @discardableResult
    func runNoMatchAnimation(duration: Float = 1) -> AnimationPlaybackController {
        var firstTransform = transform
        firstTransform.rotation *= simd_quatf(angle: 20 * .pi / 180, axis: [0, 1, 0])

        var secondTransform = firstTransform
        secondTransform.rotation *= simd_quatf(angle: -40 * .pi / 180, axis: [0, 1, 0])

        var thirdTransform = secondTransform
        thirdTransform.rotation *= simd_quatf(angle: 20 * .pi / 180, axis: [0, 1, 0])

        var transforms: [Transform] = []
        // Without appending the original transformation, it somehow skips the first animation
        transforms.append(transform)
        transforms.append(firstTransform)
        transforms.append(secondTransform)
        transforms.append(thirdTransform)

        let animationDefinition = SampledAnimation(frames: transforms,
                                                   frameInterval: duration / Float(transforms.count - 1),
                                                   bindTarget: .transform)
        let animationResource = try! AnimationResource.generate(with: animationDefinition)
        return playAnimation(animationResource)
    }

    @discardableResult
    func reveal(duration: TimeInterval = 0.5, soundEnabled: Bool = true, delay: TimeInterval = 0) -> AnimationPlaybackController {
        if let material = bottomImageMaterial {
            model?.materials[3] = material
        }

        card.isFaceUp = true

        if #available(iOS 17.0, *) {
            accessibilityLabelKey = card.imageAccessibilityLabel
        } else if let imageAccessibilityLabel = card.imageAccessibilityLabel{
            accessibilityLabel = String(localized: imageAccessibilityLabel)
        }

        if card.orientation == .horizontal && card.isBottomImageVisible {
            imageEntity?.reveal(duration: duration)
        }

        var transform = transform
        transform.translation.y = (card.orientation == .vertical ? 0 : 1) * card.width / 2
        transform.rotation = simd_quatf(angle: card.orientation == .vertical ? 1.5 * .pi : .pi, axis: [1, 0, 0])

        let playbackController = animateTransform(to: transform, duration: duration, delay: delay)

        animationCompletionHandler = self.animationCompletion { [weak self] event in
            if event.playbackController == playbackController {
                self?.move(to: transform, relativeTo: self?.parent, duration: 0.1)
            }
        }
        
        if soundEnabled {
            playSelectionSound()
        }

        return playbackController
    }

    @discardableResult
    func hide(duration: TimeInterval = 0.5, delay: TimeInterval = 0) -> AnimationPlaybackController {
        card.isFaceUp = false
        card.isMatched = false
        
        // Workaround - Apple Internal Issue?
        // With more than 50 entities it seems like the direct touch input does not work anymore if you have more than 1 particle emitter enabled
        // Therefore, the celebration particles always get toggled so the direct touch still works with more than 50 cards
        celebrationParticleEntity.isEnabled = false

        if #available(iOS 17.0, *) {
            accessibilityLabelKey = card.accessibilityLabel
        } else if let accessibilityLabel = card.accessibilityLabel {
            self.accessibilityLabel = String(localized: accessibilityLabel)
        }

        imageEntity?.hide(duration: duration)

        var transform = transform
        transform.rotation = simd_quatf(angle: card.orientation == .vertical ? .pi / 2 : 0, axis: [1, 0, 0])
        transform.rotation *= simd_quatf(angle: Float.random(in: 0...(2 * .pi)), axis: [0, 1, 0])
        transform.translation.y = 0

        let playbackController = animateTransform(to: transform, duration: duration, delay: delay)

        animationCompletionHandler = self.animationCompletion { [weak self] event in
            if event.playbackController == playbackController {
                if let material = self?.topImageMaterial {
                    self?.model?.materials[3] = material
                }
#if os(visionOS)
                self?.hideParticleEntity.particleEmitter?.isEmitting = true
                self?.hideParticleEntity.particleEmitter?.restart()
#endif


                self?.animationCompletionHandler = nil
            }
        }

        return playbackController
    }

    // MARK: - Sounds
    
    func enableSounds() {
        audioComponent.isSoundEnabled = true
    }

    func disableSounds() {
        stopAllAudio()
        audioComponent.isSoundEnabled = false
    }

    func playSelectionSound() {
        guard let selectionSound = card.selectionSound else { return }
        playAudio(name: selectionSound.name, gain: selectionSound.gain)
    }

    func playMatchingSound() {
        guard let matchingSound = card.matchingSound else { return }
        playAudio(name: matchingSound.name, gain: matchingSound.gain)
    }

    func isRegenerationNecessary(for newCard: CardComponent) -> Bool {
        return !card.hasSameMesh(as: newCard)
    }
}
