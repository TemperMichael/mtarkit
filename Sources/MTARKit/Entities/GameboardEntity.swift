//
//  GameboardEntity.swift
//
//
//  Created by Michael Temper on 05.04.23.
//

import RealityKit
import UIKit

public class GameboardEntity: Entity, HasModel, HasCollision, HasAudio, HasNavigation {

    public var gameBoard: GameboardComponent {
        get { components[GameboardComponent.self] ?? GameboardComponent() }
        set { components[GameboardComponent.self] = newValue }
    }

    public var cards: [CardEntity] = []
    private var previousCardComponents: [CardComponent] = []
    private var occlusionModel: ModelEntity?
    private var previousBackgroundMusicName: String?
    private var cachedTextures: [String: MaterialParameters.Texture] = [:]

    public init(
        rows: Int,
        columns: Int,
        orientation: Orientation,
        offset: OrientationOffset? = nil,
        gameFinishedSound: ARAudioFile? = nil,
        backgroundMusic: ARAudioFile? = nil,
        cardComponents: [CardComponent],
        audioResources: [String: ARAudioFileResource] = [:],
        cachedTextures: [String: MaterialParameters.Texture] = [:]
    ) throws {
        super.init()
        audioComponent.audioResources = audioResources
        self.cachedTextures = cachedTextures

        guard rows * columns <= cardComponents.count else {
            throw GameboardEntityError.incorrectRowsAndColumns
        }

        configure(with: rows,
                  columns: columns,
                  orientation: orientation,
                  offset: offset,
                  gameFinishedSound: gameFinishedSound,
                  backgroundMusic: backgroundMusic,
                  cardComponents: cardComponents)
    }

    public func configure(
        with rows: Int,
        columns: Int,
        orientation: Orientation,
        offset: OrientationOffset? = nil,
        gameFinishedSound: ARAudioFile? = nil,
        backgroundMusic: ARAudioFile? = nil,
        cardComponents: [CardComponent]
    ) {
        let orientationChanged = gameBoard.orientation != orientation

        gameBoard = GameboardComponent(rows: rows,
                                       columns: columns,
                                       orientation: orientation,
                                       gameFinishedSound: gameFinishedSound,
                                       backgroundMusic: backgroundMusic)

        if let offset {
            setOffset(orientation == .vertical ? offset.vertical : offset.horizontal)
        }

        if orientationChanged {
            setCardsRotation(to: orientation)
        }

        if previousCardComponents != cardComponents {
            refreshCards(with: cardComponents)
        } else if cards.count >= cardComponents.count {
            cardComponents.enumerated().filter({ $1.isBottomImageVisible != cards[$0].card.isBottomImageVisible }).forEach { index, card in
                cards[index].card = card

                cards[index].configure(with: cardComponents[index],
                                       cachedTexture: cachedTextures[cardComponents[index].bottomImageName])
            }
            
            cardComponents.enumerated().filter({ $1.isFaceUp != cards[$0].card.isFaceUp }).forEach { index, card in
                cards[index].card = card

                flipCardToCurrentState(card: card, at: index)
            }
        }

        previousCardComponents = cardComponents
    }

    @MainActor required init() {
        fatalError("init() has not been implemented")
    }
}

public extension GameboardEntity {

    func setOffset(_ offset: OptionalVector?, animationDuration: Double = 1) {
        var transform = transform

        if let x = offset?.x { transform.translation.x = x }
        if let y = offset?.y { transform.translation.y = y }
        if let z = offset?.z { transform.translation.z = z }

        move(to: transform, relativeTo: parent, duration: animationDuration)
    }

    func setCardsRotation(to orientation: Orientation, animationDuration: Double = 1) {
        cards.forEach { $0.card.orientation = orientation }
        setTransform(of: &cards)

        cards.forEach {
            if var transform = $0.parent?.transform {
                transform.translation = $0.card.position * $0.card.scale
                transform.scale = $0.card.scale
                $0.parent?.animateTransform(to: transform, duration: animationDuration)
            }

            var cardTransform = $0.transform
            if $0.card.orientation == .vertical {
                cardTransform.translation.y = 0
                cardTransform.rotation = simd_quatf(angle: $0.card.isFaceUp ? 1.5 * .pi : .pi / 2, axis: [1, 0, 0])
            } else {
                cardTransform.translation.y = $0.card.isFaceUp ? $0.card.width / 2 : 0
                cardTransform.rotation = simd_quatf(angle: $0.card.isFaceUp ? .pi : 0, axis: [1, 0, 0])
            }

            cardTransform.rotation *= simd_quatf(angle: Float.random(in: 0...(2 * .pi)), axis: [0, 1, 0])
            $0.animateTransform(to: cardTransform, duration: animationDuration)
        }
    }

    func addCards(animationDuration: CGFloat = 0) {
        cards.filter({ $0.isEnabled }).enumerated().forEach { index, card in
            let pivotEntity: PivotEntity

            if let cardPartent = card.parent as? PivotEntity {
                pivotEntity = cardPartent
            } else {
                pivotEntity = PivotEntity(entity: card, pivotPoint: .centerBottomCenter)
                addChild(pivotEntity)
            }

            var transform = pivotEntity.transform
            transform.scale = card.card.scale
            transform.translation = card.card.position * card.card.scale
            pivotEntity.animateTransform(to: transform, duration: animationDuration)

            var cardTransform = card.transform
            cardTransform.scale = .one
            cardTransform.rotation = simd_quatf(angle: card.card.orientation == .vertical ? .pi / 2 : 0, axis: [1, 0, 0]) * simd_quatf(angle: Float.random(in: 0...(2 * .pi)), axis: [0, 1, 0])
            let playbackController = card.animateTransform(to: cardTransform, duration: animationDuration)

            // When the game board is added, there is no scene yet, but still the cards have to be positioned correctly
            if scene == nil {
                if card.card.isFaceUp {
                    card.reveal(soundEnabled: false)
                } else {
                    card.hide()
                }
            } else {
                card.animationCompletionHandler = card.animationCompletion { event in
                    if event.playbackController == playbackController {
                        if card.card.isFaceUp {
                            card.reveal(soundEnabled: false)
                        } else {
                            card.hide()
                        }
                    }
                }
            }
        }
    }

    func removeCards(animationDuration: CGFloat = 0) {
        cards.forEach { card in
            var transform = card.transform
            transform.scale = .init(repeating: 0.0001)
            transform.rotation = simd_quatf(angle: .pi / 2, axis: [1, 0, 0])
            card.animateTransform(to: transform, duration: animationDuration)
        }
    }

    func flipCardToCurrentState(card: CardComponent, at index: Int) {
        if card.isFaceUp {
            let playbackController = cards[index].reveal()

            if !previousCardComponents[index].isMatched && cards[index].isMatched {
                cards[index].animationCompletionHandler = cards[index].animationCompletion { [weak self] event in
                    if event.playbackController == playbackController {
                        if self?.previousCardComponents.filter({ !$0.isFaceUp }).count == 0 {
                            self?.runGameFinishedAnimation()
                            self?.previousCardComponents.removeAll()
                        } else {
                            self?.cards.filter({ $0.card.bottomImageName == card.bottomImageName }).forEach {
                                $0.runMatchAnimation()
                            }
                        }
                    }
                }
            }
        } else if previousCardComponents[index].isFaceUp && !cards[index].isMatched {
            let playbackController = cards[index].runNoMatchAnimation(duration: 0.5)
            cards[index].animationCompletionHandler = cards[index].animationCompletion { [weak self] event in
                if event.playbackController == playbackController {
                    self?.cards[index].hide()
                }
            }
        } else {
            cards[index].hide()
        }
    }

    func runGameFinishedAnimation() {
        playGameFinishedSound()

        cards.forEach {
            $0.runMatchAnimation(soundEnabled: false)
        }
    }

    func refreshCards(with cardComponents: [CardComponent], animationDuration: CGFloat = 1) {
        previousCardComponents.removeAll()

        removeCards(animationDuration: animationDuration)
        generateCards(from: cardComponents)
        addCards(animationDuration: animationDuration)
    }

    func generateCards(from cardComponents: [CardComponent]) {
        for index in 0...cardComponents.count - 1 {
            var floatingImageComponent: ImageComponent?
            if let floatingImageName = cardComponents[index].floatingImageName {
                floatingImageComponent = ImageComponent(imageName: floatingImageName,
                                                        width: cardComponents[index].width,
                                                        height: cardComponents[index].depth,
                                                        padding: 5,
                                                        color: cardComponents[index].floatingImageColor)
            }

            if cards.count - 1 < index {
                let cardEntity = generateCard(component: cardComponents[index],
                                              floatingImageComponent: floatingImageComponent,
                                              cachedTextures: [
                                                cardComponents[index].bottomImageName: cachedTextures[cardComponents[index].bottomImageName],
                                                cardComponents[index].topImageName: cachedTextures[cardComponents[index].topImageName]
                                              ])
                cards.append(cardEntity)
            } else if cards[index].isRegenerationNecessary(for: cardComponents[index]) {
                let cardEntity = generateCard(component: cardComponents[index],
                                              floatingImageComponent: floatingImageComponent,
                                              cachedTextures: [
                                                cardComponents[index].bottomImageName: cachedTextures[cardComponents[index].bottomImageName],
                                                cardComponents[index].topImageName: cachedTextures[cardComponents[index].topImageName]
                                              ])
                
                cards[index] = cardEntity
            } else {
                cards[index].configure(with: cardComponents[index],
                                       cachedTexture: cachedTextures[cardComponents[index].bottomImageName])
                cards[index].isEnabled = true
            }
        }

        if cardComponents.count < cards.count {
            for index in cardComponents.count...cards.count - 1 {
                cards[index].isEnabled = false
            }
        }

        setTransform(of: &cards)
    }

    func generateCard(component: CardComponent,
                      floatingImageComponent: ImageComponent? = nil,
                      cachedTextures: [String: MaterialParameters.Texture?] = [:]) -> CardEntity {
        let cardMesh = MeshResource.generateBox(width: component.width,
                                                height: component.height,
                                                depth: component.depth,
                                                cornerRadius: component.cornerRadius,
                                                splitFaces: true)
        let card = CardEntity(card: component,
                              mesh: cardMesh,
                              floatingImageComponent: floatingImageComponent,
                              cachedTextures: cachedTextures)

        if let selectionSound = component.selectionSound {
            card.addAudio(audioComponent.audioResources[selectionSound.name], name: selectionSound.name)
        }

        if let matchingSound = component.matchingSound {
            card.addAudio(audioComponent.audioResources[matchingSound.name], name: matchingSound.name)
        }

        card.generateCollisionShapes(recursive: true)
        return card
    }

    func setTransform(of cards: inout [CardEntity]) {
        if let maxWidth = cards.filter({ $0.isEnabled }).sorted(by: { $0.card.width > $1.card.width }).first?.model?.mesh.width,
           let maxDepth = cards.filter({ $0.isEnabled }).sorted(by: { $0.card.depth > $1.card.depth }).first?.model?.mesh.depth {

            for (index, card) in cards.filter({ $0.isEnabled }).enumerated() {
                let spacing: Float = maxWidth / 2

                let x = Float(index / gameBoard.columns) - Float(gameBoard.rows) / 2 + 0.5
                let z = Float(index % gameBoard.columns) - Float(gameBoard.columns) / 2 + 0.5

                if card.card.orientation == .vertical {
                    card.card.position = [z * (maxWidth + spacing), -x * (maxDepth + spacing), 0]
                } else {
                    card.card.position = [z * (maxWidth + spacing), 0, x * (maxDepth + spacing)]
                }
            }
        }
    }

    // MARK: - Sounds

    func enableSounds() {
        cards.forEach { $0.enableSounds() }
        audioComponent.isSoundEnabled = true
    }

    func disableSounds() {
        cards.forEach { $0.disableSounds() }
        audioComponent.isSoundEnabled = false
    }

    func enableMusic() {
        audioComponent.isMusicEnabled = true
        playBackgroundMusic()
    }

    func disableMusic() {
        pauseAudio(name: gameBoard.backgroundMusic?.name)
        audioComponent.isMusicEnabled = false
        previousBackgroundMusicName = nil
    }

    func playGameFinishedSound() {
        guard let gameFinishedSound = gameBoard.gameFinishedSound else { return }
        playAudio(name: gameFinishedSound.name, gain: gameFinishedSound.gain)
    }

    func playBackgroundMusic() {
        guard let backgroundMusic = gameBoard.backgroundMusic, previousBackgroundMusicName != backgroundMusic.name, parent != nil, audioComponent.isMusicEnabled else {
            return
        }

        pauseAudio(name: previousBackgroundMusicName)
        continueAudio(name: backgroundMusic.name, gain: backgroundMusic.gain)
        previousBackgroundMusicName = backgroundMusic.name
    }
}
