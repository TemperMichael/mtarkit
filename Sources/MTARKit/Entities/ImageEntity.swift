//
//  ImageEntity.swift
//
//
//  Created by Michael Temper on 07.04.23.
//

import AVFoundation
import Combine
import RealityKit
import UIKit

public class ImageEntity: Entity, HasModel  {

    public var image: ImageComponent {
        get { components[ImageComponent.self] ?? ImageComponent(imageName: "") }
        set { components[ImageComponent.self] = newValue }
    }

    public var animationCompletionHandler: Cancellable?
    public var texture: MaterialParameters.Texture?

    public init(imageComponent: ImageComponent, cachedTexture: MaterialParameters.Texture? = nil) {
        super.init()

        self.image = imageComponent

        setModel(cachedTexture: cachedTexture)
    }

    public convenience init(imageName: String, size: Float, padding: CGFloat = 20, color: UIColor = .white, cachedTexture: MaterialParameters.Texture? = nil) {
        self.init(imageName: imageName, width: size, height: size, padding: padding, color: color, cachedTexture: cachedTexture)
    }

    public init(imageName: String, width: Float = 0.05, height: Float = 0.05, padding: CGFloat = 20, color: UIColor = .white, cachedTexture: MaterialParameters.Texture? = nil) {
        super.init()

        image = ImageComponent(imageName: imageName, width: width, height: height, padding: padding, color: color)

        setModel(cachedTexture: cachedTexture)
    }

    public func changeImage(component: ImageComponent, cachedTexture: MaterialParameters.Texture? = nil) {
        self.image = component

        guard let texture = cachedTexture ?? getTexture() else {
            print("Could not change ImageEntity image - Texture not found: \(component.imageName)")
            return
        }

        var unlitMaterial = UnlitMaterial(color: .clear)
        unlitMaterial.opacityThreshold = 0

        let material = getMaterial(for: texture)
        model?.materials = [material,
                            unlitMaterial,
                            material,
                            unlitMaterial,
                            unlitMaterial,
                            unlitMaterial,
                            unlitMaterial]
    }

    private func getTexture() -> MaterialParameters.Texture? {
        guard let finalImage = (UIImage(named: image.imageName) ?? UIImage(systemName: image.imageName)?.withTintColor(.white, renderingMode: .alwaysOriginal))?.withPadding(image.padding), let cgImage = finalImage.cgImage else {
            return nil
        }

        do {
            texture = .init(try .generate(from: cgImage, options: .init(semantic: .hdrColor)))
            return texture
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }

    private func getMaterial(for texture: MaterialParameters.Texture) -> Material {
        var material = UnlitMaterial(color: image.color.withAlphaComponent(0.9999999))
        material.color = .init(tint: image.color.withAlphaComponent(0.9999999),
                               texture: texture)
        material.opacityThreshold = 0
        return material
    }

    public func setModel(cachedTexture: MaterialParameters.Texture? = nil) {
        guard let texture = cachedTexture ?? getTexture() else {
            print("Texture not found - no model for ImageEntity set")
            return
        }

        let material = getMaterial(for: texture)
        let aspectFitRect = AVMakeRect(aspectRatio: CGSize(width: texture.resource.width,
                                                     height: texture.resource.height),
                                 insideRect: CGRect(x: 0,
                                                    y: 0,
                                                    width: CGFloat(image.width),
                                                    height: CGFloat(image.height)))

        let imageMesh = MeshResource.generateBox(width: Float(aspectFitRect.width),
                                                 height: Float(aspectFitRect.height),
                                                 depth: 0,
                                                 splitFaces: true)

        var unlitMaterial = UnlitMaterial(color: .clear)
        unlitMaterial.opacityThreshold = 0

        let materials = [material,
                         unlitMaterial,
                         material,
                         unlitMaterial,
                         unlitMaterial,
                         unlitMaterial,
                         unlitMaterial]
        model = ModelComponent(mesh: imageMesh, materials: materials)

        deferredRotationAngle = -.pi
        deferredRotationAxis = [0, 1, 0]

        scale = .zero
    }

    required init() {
        super.init()

        setModel()
    }
}

extension ImageEntity: HasLookAt {}

public extension ImageEntity {

    @discardableResult
    func reveal(duration: TimeInterval = 0.5) -> AnimationPlaybackController {
        let aspectFitRect = AVMakeRect(aspectRatio: CGSize(width: texture?.resource.width ?? 1,
                                                     height: texture?.resource.height ?? 1),
                                 insideRect: CGRect(x: 0,
                                                    y: 0,
                                                    width: CGFloat(image.width),
                                                    height: CGFloat(image.height)))

        return scale(to: [
            (model?.mesh.height ?? 1) / Float(aspectFitRect.height),
            (model?.mesh.width ?? 1) / Float(aspectFitRect.width),
            1
        ], duration: duration)
    }

    @discardableResult
    func hide(duration: TimeInterval = 0.5) -> AnimationPlaybackController {
        return scale(to: .zero, duration: duration)
    }
}
