//
//  ARAudioFileResource.swift
//  
//
//  Created by Michael Temper on 23.05.23.
//

import RealityKit

public struct ARAudioFileResource {
    let resource: AudioFileResource
    let kind: ARAudioKind

    public init(resource: AudioFileResource, kind: ARAudioKind) {
        self.resource = resource
        self.kind = kind
    }
}
