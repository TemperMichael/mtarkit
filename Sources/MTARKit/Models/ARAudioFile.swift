//
//  ARAudioFile.swift
//  
//
//  Created by Michael Temper on 22.05.23.
//

import Foundation
import RealityKit

#if !os(visionOS)
public typealias ARInputMode = AudioResource.InputMode
#else
public typealias ARInputMode = PlaceholderInputMode

public enum PlaceholderInputMode: Codable {
    case ambient
    case nonSpatial
    case spatial
}
#endif

/**
 Contains all configurations for any kind of audio.
 - parameter name: The name of the audio.
 - parameter mode: Modes for processing audio resources. On `visionOS` this is a simple placeholder for reusing the same initializer.
 - parameter loop: Defines if the audio loops.
 - parameter type: The raw file type like `mp3`.
 - parameter kind: The kind of the audio.
 - parameter name: The name of the audio.
 - parameter gain: The overall level for all sounds emitted from an entity. In relative Decibels, in the range  `-.infinity ... .zero` where `.zero` is the default.
 */
public struct ARAudioFile: Codable {
    public var name: String
    public var mode: ARInputMode
    public var loop: Bool
    public var type: ARAudioFileType
    public var kind: ARAudioKind
    public var gain: Double

    public init(name: String, mode: ARInputMode, loop: Bool = false, type: ARAudioFileType, kind: ARAudioKind, gain: Double = .zero) {
        self.name = name
        self.mode = mode
        self.loop = loop
        self.type = type
        self.kind = kind
        self.gain = gain
    }

    public var fullName: String { name + "." + type.rawValue }
}
