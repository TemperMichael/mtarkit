//
//  OrientationOffset.swift
//  PloppyMatchingPairs
//
//  Created by Michael Temper on 27.08.23.
//

import Foundation

public struct OrientationOffset: Codable {
    let vertical: OptionalVector?
    let horizontal: OptionalVector?

    public init(vertical: OptionalVector? = nil, horizontal: OptionalVector? = nil) {
        self.vertical = vertical
        self.horizontal = horizontal
    }
}
