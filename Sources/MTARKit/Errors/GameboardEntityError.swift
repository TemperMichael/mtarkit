//
//  GameboardEntityError.swift
//  
//
//  Created by Michael Temper on 25.04.23.
//

import Foundation

public enum GameboardEntityError: Error {
    case incorrectRowsAndColumns
}

extension GameboardEntityError: LocalizedError {

    public var errorDescription: String? {
        switch self {
        case .incorrectRowsAndColumns:
            return String(localized: "entity.error.gameboard.description",
                          bundle: .module,
                          comment: "The error message when a Gameboard entity has an incorrect amount of rows and columns")
        }
    }
}
