//
//  NavigationSystem.swift
//  
//
//  Created by Michael Temper on 10.06.23.
//

import RealityKit
import UIKit

public class NavigationSystem: System {

    private static let query = EntityQuery(where: .has(NavigationComponent.self))

    private var additionalXRotation: Float = 0
    private var lastObservationTimestamp = Date()
    private var visualNavigationElement: ModelEntity?

    private var targetState: NavigationTargetState = .visible {
        didSet {
            UIAccessibility.post(notification: .announcement,
                                 argument: targetState.accessibilityAnnouncementIdentifier.localized())
        }
    }

    required public init(scene: RealityKit.Scene) { }

    public func update(context: SceneUpdateContext) {
        context.scene.performQuery(Self.query).forEach { entity in
            guard let component = entity.components[NavigationComponent.self] as? NavigationComponent, component.isActive else {
                return
            }

            setNavigationElement(for: entity, with: component, in: context)

            guard Date().timeIntervalSince(lastObservationTimestamp) > (UIAccessibility.isVoiceOverRunning ? 3 : 1) else { return }
            lastObservationTimestamp = Date()

            setTargetState(for: entity, with: component)
        }
    }

    func setNavigationElement(for entity: Entity, with component: NavigationComponent, in context: SceneUpdateContext) {
        if let visualNavigationElement = visualNavigationElement {
            if targetState == .visible {
                if visualNavigationElement.parent != nil {
                    visualNavigationElement.removeFromParent()
                }
            } else {
                if visualNavigationElement.parent == nil {
                    entity.parent?.addChild(visualNavigationElement)
                }

                var forwardVector = component.currentCameraTransform.forwardVector
                visualNavigationElement.look(at: entity.position(relativeTo: nil),
                                             from: component.currentCameraTransform.position + forwardVector,
                                             relativeTo: nil)
                visualNavigationElement.setPosition(component.currentCameraTransform.position + forwardVector, relativeTo: nil)
                visualNavigationElement.transform.rotation *= simd_quatf(angle: .pi / 2, axis: [0, 1, 0])
                visualNavigationElement.transform.rotation *= simd_quatf(angle: additionalXRotation, axis: [1, 0, 0])
            }
        } else if let visualNavigationElement = component.visualNavigationEntity {
            self.visualNavigationElement = visualNavigationElement
        }
    }

    func setTargetState(for entity: Entity, with component: NavigationComponent) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }

            let cameraToGameboard = entity.position(relativeTo: nil) - component.currentCameraTransform.position
            let cross = cross(cameraToGameboard, component.currentCameraTransform.forwardVector)
            let k = sqrt(Float(length_squared(cameraToGameboard).magnitude * length_squared(component.currentCameraTransform.forwardVector).magnitude))
            let kCosTheta = dot(cameraToGameboard, component.currentCameraTransform.forwardVector)
            let w = k + kCosTheta

            let quaternion = simd_quaternion(cross.x, cross.y, cross.z, w).normalized
            let verticalAngle = cameraToGameboard.z > 0 ? -quaternion.imag.x : quaternion.imag.x
            let horizontalAngle = quaternion.imag.y

            let distance = length(cameraToGameboard).magnitude
            let boundingRadius = entity.visualBounds(relativeTo: nil).boundingRadius
            let scale = boundingRadius / distance

            if abs(horizontalAngle) > 0.3 && scale < 0.85 {
                targetState = horizontalAngle > 0 ? .left : .right
                additionalXRotation = 0
                return
            }

            if abs(verticalAngle) > 0.3 && scale < 0.85 {
                targetState = verticalAngle > 0 ? .up : .down
                additionalXRotation = .pi / 2
                return
            }

            if scale > 0.85 && UIAccessibility.isVoiceOverRunning {
                targetState = .tooClose
            } else if scale < (UIAccessibility.isVoiceOverRunning ? 0.4 : 0.2) {
                targetState = .away
            } else if targetState != .visible {
                targetState = .visible
                additionalXRotation = 0
            }
        }
    }
}
