//
//  LookAtSystem.swift
//
//
//  Created by Michael Temper on 14.04.23.
//

import RealityKit

public class LookAtSystem: System {

    private static let query = EntityQuery(where: .has(LookAtComponent.self))

    required public init(scene: RealityKit.Scene) { }

    public func update(context: SceneUpdateContext) {
        context.scene.performQuery(Self.query).forEach { entity in
            guard let component = entity.components[LookAtComponent.self] as? LookAtComponent, component.isActive else {
                return
            }
            
            entity.look(at: component.lookDestination, from: entity.position(relativeTo: nil), relativeTo: nil)
            entity.transform.rotation *= simd_quatf(angle: component.deferredRotationAngle,
                                                    axis: component.deferredRotationAxis)
        }
    }
}
