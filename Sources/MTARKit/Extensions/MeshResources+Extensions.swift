//
//  MeshResources+Extensions.swift
//
//
//  Created by Michael Temper on 05.04.23.
//

import RealityKit

public extension MeshResource {

    var width: Float { bounds.max.x - bounds.min.x }
    var height: Float { bounds.max.y - bounds.min.y }
    var depth: Float { bounds.max.z - bounds.min.z }
}
