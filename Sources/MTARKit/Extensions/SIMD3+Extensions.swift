//
//  File.swift
//  
//
//  Created by Michael Temper on 19.10.23.
//

import Foundation

public extension SIMD3 {

    init(size: Scalar) {
        self = [size, size, size]
    }
}
