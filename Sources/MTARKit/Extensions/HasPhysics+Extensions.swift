//
//  HasPhysics+Extensions.swift
//
//
//  Created by Michael Temper on 19.10.23.
//

import RealityKit

public extension HasPhysics {

    func stopMotion() {
        physicsMotion?.angularVelocity = .zero
        physicsMotion?.linearVelocity = .zero
    }

    func startRandomMotion(linearRange: Float = 0.1, angularRange: Float = 1) {
        physicsMotion?.linearVelocity = [
            Float.random(in: -linearRange...linearRange),
            Float.random(in: -linearRange...linearRange),
            Float.random(in: -linearRange...linearRange),
        ]

        physicsMotion?.angularVelocity = [
            Float.random(in: -angularRange...angularRange),
            Float.random(in: -angularRange...angularRange),
            Float.random(in: -angularRange...angularRange),
        ]
    }
}
