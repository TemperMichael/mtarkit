//
//  ARView+Extensions.swift
//
//
//  Created by Michael Temper on 23.04.23.
//

import ARKit
import RealityKit

#if !os(visionOS)
extension ARView: ARCoachingOverlayViewDelegate {
    
    public func addCoaching() {
        let coachingOverlay = ARCoachingOverlayView()
        coachingOverlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        coachingOverlay.goal = .tracking
        coachingOverlay.session = session
        coachingOverlay.delegate = self
        addSubview(coachingOverlay)
    }
}
#endif
