//
//  SimdFloat4x4+Extensions.swift
//  
//
//  Created by Michael Temper on 10.06.23.
//

import RealityKit

public extension simd_float4x4 {

    var forwardVector: SIMD3<Float> { SIMD3<Float>(-columns.2.x, -columns.2.y, -columns.2.z) }
    var position: SIMD3<Float> { SIMD3<Float>(columns.3.x, columns.3.y, columns.3.z) }
}
