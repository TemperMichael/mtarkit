//
//  Size3D+Extensions.swift
//
//
//  Created by Michael Temper on 19.10.23.
//

import Spatial

public extension Size3D {

    init(size: Float) {
        self = .init(width: size, height: size, depth: size)
    }
}
