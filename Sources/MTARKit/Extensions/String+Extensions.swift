//
//  String+Extensions.swift
//  
//
//  Created by Michael Temper on 10.06.23.
//

import Foundation

extension String {

    func localized(_ arguments: [CVarArg] = []) -> String {
        return String(format: NSLocalizedString(self, bundle: .module, comment: ""), arguments: arguments)
    }
}

