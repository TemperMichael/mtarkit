//
//  ARAudioFile+Extensions.swift
//
//
//  Created by Michael Temper on 10.08.23.
//

import RealityKit

#if !os(visionOS)
extension AudioResource.InputMode: Codable {

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self)
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self = try container.decode(Self.self)
    }
}
#endif
