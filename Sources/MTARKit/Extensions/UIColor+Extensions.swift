//
//  UIColor+Extensions.swift
//
//
//  Created by Michael Temper on 05.04.23.
//

import UIKit

public extension UIColor {
    
    func adjust(hue: CGFloat = 0, saturation: CGFloat = 0, brightness: CGFloat = 0) -> UIColor {
        var currentHue: CGFloat = 0.0
        var currentSaturation: CGFloat = 0.0
        var currentBrigthness: CGFloat = 0.0
        var currentAlpha: CGFloat = 0.0
        
        if getHue(&currentHue, saturation: &currentSaturation, brightness: &currentBrigthness, alpha: &currentAlpha) {
            return UIColor(hue: currentHue + hue,
                           saturation: currentSaturation + saturation,
                           brightness: currentBrigthness + brightness,
                           alpha: currentAlpha)
        } else {
            return self
        }
    }

    static func random() -> UIColor {
        let colors: [UIColor] = [
            .blue,
            .red,
            .green,
            .yellow,
            .orange,
            .brown,
            .white,
            .cyan,
            .magenta,
            .purple,
            .systemIndigo,
            .systemTeal,
            .systemMint,
            .systemPink
        ]

        return colors.randomElement()!
    }
}
