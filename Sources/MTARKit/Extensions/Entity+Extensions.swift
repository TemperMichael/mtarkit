//
//  File.swift
//  
//
//  Created by Michael Temper on 17.10.23.
//

import Foundation
import Combine
import RealityKit
import Spatial

public extension Entity {
    
#if os(visionOS)
    var particleEmitter: ParticleEmitterComponent? {
        get { components[ParticleEmitterComponent.self] }
        set { components[ParticleEmitterComponent.self] = newValue }
    }
#endif

    func setRandomPosition(in size: Size3D, offset: SIMD3<Float> = .zero, safeAreaInset: Float = 0.2) {
        let divider = (2 + safeAreaInset)

        transform.translation = [
            Float.random(in: -(Float(size.width) / divider)...Float(size.width) / divider) + offset.x,
            Float.random(in: -(Float(size.height) / divider)...Float(size.height) / divider) + offset.y,
            Float.random(in: -(Float(size.depth) / divider)...Float(size.depth) / divider) + offset.z
        ]
    }

    @discardableResult
    func animateTransform(to newTransform: Transform, duration: CGFloat = 1, timing: AnimationTimingFunction = .easeInOut, delay: TimeInterval = .zero) -> AnimationPlaybackController {
        let animationDefinition = FromToByAnimation(to: newTransform,
                                                    duration: duration,
                                                    timing: timing,
                                                    bindTarget: .transform,
                                                    delay: delay)
        
        let animationResource = try! AnimationResource.generate(with: animationDefinition)
        let playbackController = playAnimation(animationResource)
        return playbackController
    }

    @discardableResult
    func scale(to scale: SIMD3<Float>, duration: CGFloat = 1, timing: AnimationTimingFunction = .easeInOut) -> AnimationPlaybackController {
        var transform = transform
        transform.scale = scale
        return animateTransform(to: transform, duration: duration, timing: timing)
    }

    func animationCompletion(handler: @escaping  (AnimationEvents.PlaybackCompleted) -> Void) -> Cancellable? {
        return scene?.subscribe(to: AnimationEvents.PlaybackCompleted.self, on: self, handler)
    }
}
