import XCTest
import RealityKit
@testable import MTARKit

final class MTARKitTests: XCTestCase {

    func testRevealCard() {
        let component = CardComponent()
        let entity = CardEntity(card: component)
        XCTAssertFalse(entity.card.isFaceUp)

        entity.reveal(duration: 0)
        XCTAssertTrue(entity.card.isFaceUp)

        // Check that the card stays revealed, even when it's already revealed and does not get toggled
        entity.reveal(duration: 0)
        XCTAssertTrue(entity.card.isFaceUp)
    }

    func testHideCard() {
        let component = CardComponent(isFaceUp: true, isMatched: true)
        let entity = CardEntity(card: component)

        let imageComponent = ImageComponent(imageName: "circle.fill")
        entity.configure(with: imageComponent)
        XCTAssertTrue(entity.card.isFaceUp, "The initialization of a CardEntity`s isFaceUp is incorrect.")
        XCTAssertTrue(entity.card.isMatched, "The initialization of a CardEntity`s isMatched is incorrect.")

        entity.hide(duration: 0)
        XCTAssertFalse(entity.card.isFaceUp, "A CardEntity`s isFaceUp is incorrect after being hidden.")
        XCTAssertFalse(entity.card.isMatched, "A CardEntity`s isMatched is incorrect after being hidden.")

        _ = XCTWaiter.wait(for: [expectation(description: "Wait for 0.2 seconds")], timeout: 0.2)
        XCTAssertTrue(entity.imageEntity?.isLookAtComponentActive == true, "A CardEntity`s ImageEntity LookAtSystem is not activated after being hidden.")

        // Check that the card stays hidden, even when it's already hidden and does not get toggled
        entity.hide(duration: 0)
        XCTAssertFalse(entity.card.isFaceUp, "A CardEntity`s isFaceUp gets toggled instead of being set to false when it gets hidden.")
        XCTAssertFalse(entity.card.isMatched, "A CardEntity`s isMatched gets toggled instead of being set to false when it gets hidden.")

        _ = XCTWaiter.wait(for: [expectation(description: "Wait for 0.2 seconds")], timeout: 0.2)
        XCTAssertTrue(entity.imageEntity?.isLookAtComponentActive == true, "A CardEntity`s ImageEntity LookAtSystem is not activated after being hidden.")
    }

    func testTooFewCardsGameboard() {
        do {
            _ = try GameboardEntity(rows: 10, columns: 20, orientation: .horizontal, cardComponents: [])
            XCTFail("No error was thrown when rows and columns are higher than the amount of card components.")
        } catch {
            print(error.localizedDescription)
        }
    }

    func testInitGameboard() {
        let rows = 2
        let columns = 3
        let cardComponents = Array(repeating: CardComponent(), count: rows * columns)
        guard let entity = try? GameboardEntity(rows: rows, columns: columns, orientation: .horizontal, cardComponents: cardComponents) else {
            XCTFail("Gameboard Entity initialization failed.")
            return
        }

        XCTAssertEqual(entity.gameBoard.rows, rows, "Number of rows were not set correctly in Gameboard Entity.")
        XCTAssertEqual(entity.gameBoard.columns, columns, "Number of columns were not set correctly in Gameboard Entity.")

        _ = XCTWaiter.wait(for: [expectation(description: "Wait for 0.6 seconds")], timeout: 0.6)
        XCTAssertEqual(entity.cards.count, cardComponents.count, "Number of cards is incorrect in gameboard.")
    }

    func testInitImageEntity() {
        let component = ImageComponent(imageName: "circle.fill")
        let entity = ImageEntity(imageComponent: component)

        XCTAssertEqual(entity.image, component, "ImageComponent was set incorrectly in ImageEntity.")
    }

    func testInitPivotEntity() {
        let childEntity = CardEntity(card: CardComponent())
        let _ = PivotEntity(entity: childEntity, pivotPoint: .leadingBottomFront)

        guard let mesh = childEntity.model?.mesh else {
            XCTFail("PivotEntity's child mesh not available.")
            return
        }
        
        XCTAssertEqual(childEntity.position,
                       PivotPoint.leadingBottomFront.rawValue * -[mesh.width / 2, mesh.height / 2, mesh.depth / 2],
                       "ChildEntity's pivot point is on the wrong position.")
    }
}
